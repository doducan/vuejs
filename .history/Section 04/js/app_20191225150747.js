new Vue({
    el: '#app',
    data: {
        playerHealth: 100,
        monsterHealth: 100,
        gameIsRunning: false,
        turns:[]
    },
    methods: {
        StartNewGame: function () {
            this.gameIsRunning = true;
            this.playerHealth = 100;
            this.monsterHealth = 100;
        },
        attack: function () {
            //Monster
            damage= this.inputdamage(4,10);
            this.monsterHealth -= damage;
            this.turns.unshift({
               isPlayer:true,
                textLog:'Player hits  Monster for ' +damage
            });
            if(this.checkplayeroptions()){
                return;
            }
            //Player
           this.monsterdamage();
        },
        specialAttack: function ()
        {
            //Monster
            damage= this.inputdamage(10,20);
            this.monsterHealth -= damage;
            this.turns.unshift({
                isPlayer:true,
                textLog:'Player hits  Monster for ' +damage
            });
            if(this.checkplayeroptions()){
                return;
            }
            //Player
            this.monsterdamage();
        },

        heal: function ()
        {
            //Player
            if(this.playerHealth>70){
                return false;
            }else if(this.playerHealth<=60){
                this.playerHealth +=10;
            } else{
                this.playerHealth=70;
            }
            this.turns.unshift({
                isPlayer:true,
                textLog:'Player heals for 10'
            });
            //Monster
            this.monsterdamage();
        },
        giveUp: function () {
               this.StartNewGame ();
                this.turns =[];
                alert('You Lost!');
        },
        monsterdamage:function(){
            damage=this.inputdamage(5, 12);
            this.playerHealth -= damage;
            this.turns.unshift({
                isPlayer:false,
                textLog:'Monster hits  Player for ' +damage
            });
            this.checkplayeroptions();
        },
        inputdamage: function (mindamage, maxdamage) {
            return Math.max(Math.floor(Math.random() * maxdamage) + 1, mindamage);
        },
        checkplayeroptions: function ()
        {  if (this.monsterHealth <= 0) {
                if (confirm('You won !New game ?')) {
                    this.StartNewGame();
                    this.turns =[];
                }else {
                    this.gameIsRunning=false;
                    this.turns =[];
                }
                return true;}
        else if( this.playerHealth <= 0)
        {    if (confirm('Monster won !New game ?')) {
                    this.StartNewGame();
                }else {
                    this.gameIsRunning=false;
                }
                return true;}
        },




        }


});