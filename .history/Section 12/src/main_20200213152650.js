import Vue from 'vue'
import App from './App.vue'

Vue.directive('scroll');

new Vue({
    el: '#app',
    render: h => h(App)
})