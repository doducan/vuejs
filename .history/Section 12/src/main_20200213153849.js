import Vue from 'vue'
import App from './App.vue'

Vue.directive('scroll', {
    bind(el, binding, vnode) {
        el.style.backgroundColor = 'yellow'
    }
});

new Vue({
    el: '#app',
    render: h => h(App)
})