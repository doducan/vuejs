import Vue from 'vue'
import Contact from './Contact.vue'
import App from './App.vue'
const EventBus = new Vue({
    methods: {
        changeAge(age) {
            this.$emit('ageWasEdit', age);
        }
    }
})
export default EventBus
Vue.component('contact-component', Contact);
new Vue({
    el: '#app',
    render: h => h(App)
})