import Vue from 'vue'
import Contact from './Contact.vue'
import App from './App.vue'


new Vue({
    el: '#app',
    render: h => h(App)
})