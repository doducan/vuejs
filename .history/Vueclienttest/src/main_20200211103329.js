import Vue from 'vue'
import App from './App.vue'
import Contact from './Contact.vue'

new Vue({
    el: '#app',
    render: h => h(App)
})